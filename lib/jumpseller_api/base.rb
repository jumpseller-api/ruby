require 'active_support'
require 'active_support/inflector'

module JumpsellerApi
  class Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    WHITELISTED_CODES = [400, 404].freeze

    def attributes=; end

    def self.make_request(method, uri, params, authentication)
      if authentication.connection_type == 'oauth'
        params[:headers] = {
          'Content-Type' => 'application/json',
          'Accept' => 'application/json',
          'Authorization' => "Bearer #{authentication.auth_token}"
        }
      else
        query = params[:query] || {}
        query.merge!(authentication.auth_params)
        params[:headers] = { 'Content-Type' => 'application/json' }
        params[:query] = query
      end

      res = send(method, uri, params)

      raise BannedByRateLimit unless res.headers['jumpseller-bannedbyratelimit-reset'].nil? # stop processing if banned.

      raise_exception(res) unless res.success? || WHITELISTED_CODES.include?(res.code)

      # gathering headers information from Jumspeller API.
      per_second_limit      = res.headers['jumpseller-persecondratelimit-limit'].to_i
      per_minute_limit      = res.headers['jumpseller-perminuteratelimit-limit'].to_i
      per_minute_remaining  = res.headers['jumpseller-perminuteratelimit-remaining'].to_i
      per_second_remaining  = res.headers['jumpseller-persecondratelimit-remaining'].to_i

      seconds = fetch_sleep_seconds(per_second_limit, per_second_remaining, per_minute_limit, per_minute_remaining)
      sleep seconds if ENV['ENVIRONMENT'] != 'test' && authentication.enviroment != 'test'

      res
    end

    def self.raise_exception(response)
      begin
        parsed = JSON.parse(response.response.body)
      rescue JSON::ParserError
        raise JumpsellerApi::ApiStandardError, response.response.body, [response.request, response].inspect
      end

      case response.code
      when 402
        raise JumpsellerApi::BlockedSubscription, parsed['message'], [response.request, response].inspect
      when 403
        raise JumpsellerApi::OAuth2TokenExpired, parsed['message'], [response.request, response].inspect if parsed['message'].match?(/oauth.*expired/i)
        raise JumpsellerApi::WrongOauth2TokenProvided, parsed['message'], [response.request, response].inspect if parsed['message'].match?(/wrong.*oauth.*/i)

        raise JumpsellerApi::RateLimitExceeded, parsed['message'], [response.request, response].inspect
      else
        raise JumpsellerApi::ApiStandardError, parsed['message'], [response.request, response].inspect
      end
    end

    # fetch_sleep_seconds(4, 4, 120, 120) => faster
    # fetch_sleep_seconds(4, 3, 120, 50)
    # fetch_sleep_seconds(4, 3, 120, 10)
    # fetch_sleep_seconds(4, 2, 120, 10)
    # fetch_sleep_seconds(4, 1, 120, 20)
    # fetch_sleep_seconds(4, 4, 120, 0)
    # fetch_sleep_seconds(4, 0, 120, 0) => slower
    def self.fetch_sleep_seconds(per_second_limit, per_second_remaining, per_minute_limit, per_minute_remaining)
      base_per_minute = 1.0 / per_minute_limit # 0.0083 or 0.01666 seconds, depending on plan.
      backoff_per_minute = base_per_minute**(per_minute_remaining / per_minute_limit.to_f)

      base_per_second = 1.0 / per_second_limit # 0.25 or 0.5 seconds, depending on plan.
      backoff_per_second = base_per_second**(per_second_remaining / per_second_limit.to_f)

      [backoff_per_minute, backoff_per_second].max
    end

    def save(authentication)
      return false if attributes.empty?

      response = if id
                   update(authentication)
                 else
                   create(authentication)
                 end

      return response.parsed_response if response.code == 200

      raise JumpsellerApi::CouldNotSave, response.parsed_response['message']
    end

    def update(authentication)
      make_request('put', "#{resource}/#{id}.json", { body: body }, authentication)
    end

    def create(authentication)
      make_request('post', "#{resource}.json", { body: body }, authentication)
    end

    def remove(authentication)
      return { 'message' => 'Failed' } if id.nil? || id == ''

      make_request('delete', "#{resource}/#{id}.json", {}, authentication).parsed_response
    end

    def body
      JSON.generate(entity => attributes)
    end

    def make_request(method, uri, params, authentication)
      self.class.make_request(method, uri, params, authentication)
    end

    def attributes(snapshot = nil)
      attrs = {}
      instance_variables.each do |ivar|
        attr_value = instance_variable_get(ivar)
        unless attr_value.nil?
          attr_key = /\W*(.*)/.match(ivar)[1]
          attrs[attr_key] = attr_value
        end
      end

      attrs.reject do |k, v|
        (k.to_sym == :authentication) || (k.to_sym == :snapshot) || (k.to_sym == :snapshot_variants) || (snapshot && v == snapshot[k])
      end
    end
  end

  EXCEPTION_CLASSES = %w[BlockedSubscription BannedByRateLimit OAuth2TokenExpired RateLimitExceeded].freeze
end
