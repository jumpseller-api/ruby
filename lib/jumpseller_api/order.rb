module JumpsellerApi
  class Order < JumpsellerApi::Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    attr_accessor :hash

    def self.count(authentication)
      res = make_request('get', '/orders/count.json', {}, authentication)
      res.parsed_response['count']
    end

    def self.find_all(authentication, status = nil)
      orders_count = 100 # count(authentication)
      orders = []
      page = 1
      params = { limit: 200, page: page }
      parsed_result = []
      endpoint = if status
                   "/orders/status/#{status}.json"
                 else
                   '/orders.json'
                 end
      while page == 1 || !parsed_result.empty?
        res = make_request('get', endpoint, { query: params }, authentication)
        parsed_result = res.parsed_response
        parsed_result.each do |parsed_order|
          order = new(parsed_order['order'], authentication)
          orders << order

          yield(order, orders_count) if block_given?
        end
        # set_orders(parsed_result)
        page += 1
        params[:page] = page
      end

      orders
    end

    def self.find(id, authentication)
      res = make_request('get', "/orders/#{id}.json", {}, authentication)

      return nil if res.code == 404

      new(res.parsed_response['order'], authentication)
    end

    def resource
      '/orders'
    end

    def entity
      'order'
    end

    def initialize(order, authentication)
      @authentication = authentication
      self.attributes = if order['order']
                          (order['order'])
                        else
                          order
                        end
    end

    def attributes=(order)
      @hash = order
    end
  end
end
