module JumpsellerApi
  class OAuth2TokenExpired < StandardError
  end

  class RateLimitExceeded < StandardError
  end

  class BlockedSubscription < StandardError
  end

  class BannedByRateLimit < StandardError
  end

  class ApiStandardError < StandardError
  end

  class WrongOauth2TokenProvided < StandardError
  end

  class CouldNotSave < StandardError
  end
end
