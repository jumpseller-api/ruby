module JumpsellerApi
  class Store < JumpsellerApi::Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    attr_accessor :name, :code, :currency, :country, :timezone, :email, :hooks_token,
                  :url, :logo, :subscription_plan, :weight_unit, :fb_pixel_id, :address, :subscription_status

    def self.info(authentication)
      res = make_request('get', '/store/info.json', {}, authentication)
      return nil if res.code == 403 || res.code == 405

      new(res.parsed_response)
    end

    def resource
      '/store/info'
    end

    def entity
      'store'
    end

    def initialize(store_info)
      self.attributes = (store_info['store'])
    end

    def attributes=(store_info)
      @name = store_info['name']
      @code = store_info['code']
      @currency = store_info['currency']
      @country = store_info['country']
      @timezone = store_info['timezone']
      @email = store_info['email']
      @hooks_token = store_info['hooks_token']
      @url = store_info['url']
      @logo = store_info['logo']
      @address = store_info['address']
      @subscription_plan = store_info['subscription_plan']
      @weight_unit = store_info['weight_unit']
      @fb_pixel_id = store_info['fb_pixel_id']
      @subscription_status = store_info['subscription_status']
    end
  end
end
