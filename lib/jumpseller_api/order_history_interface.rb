module JumpsellerApi
  class OrderHistoryInterface < BaseInterface
    def new(order)
      OrderHistory.new(order, @authentication)
    end
  end
end
