module JumpsellerApi
  class Category < JumpsellerApi::Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    attr_accessor :id, :name, :parent_id, :permalink

    def resource
      '/categories'
    end

    def entity
      'category'
    end

    def initialize(category, authentication)
      @authentication = authentication
      self.attributes = if category['category']
                          (category['category'])
                        else
                          category
                        end
    end

    def attributes=(category)
      @id = category['id']
      @name = category['name']
      @parent_id = category['parent_id']
      @permalink = category['permalink']
    end

    def self.find_by(category_name, authentication)
      find_all(authentication) do |category|
        return category if category.name.casecmp(category_name).zero?
      end

      nil
    end

    def self.find_or_create_by_name(category_name, authentication)
      return nil if category_name == '' || category_name.nil?

      category = find_by(category_name, authentication)

      return category if category

      category = new({ 'category' => { 'name' => category_name } }, authentication)
      category.save
      category
    end

    def save
      response = super(@authentication)
      self.attributes = (response['category'])
    end

    def self.find_all(authentication)
      categories = []
      # page = 1
      # params = {limit: 200, page: page}
      # parsed_result = []
      #
      # while page == 1 || !parsed_result.empty?
      res = make_request('get', '/categories.json', {}, authentication)
      # sleep 2
      parsed_result = res.parsed_response
      parsed_result.each do |parsed_category|
        category = new(parsed_category['category'], authentication)
        categories << category

        yield(category) if block_given?
      end
      # set_categories(parsed_result)
      # page += 1
      # params[:page] = page
      # end

      categories
    end
  end
end
