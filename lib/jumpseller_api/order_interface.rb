module JumpsellerApi
  class OrderInterface < BaseInterface
    def new(order)
      Order.new(order, @authentication)
    end

    def find(id)
      Order.find(id, @authentication)
    end

    def count
      Order.count(@authentication)
    end

    def find_all(status = nil, &block)
      if block_given?
        Order.find_all(@authentication, &block)
      else
        Order.find_all(@authentication, status)
      end
    end
  end
end
