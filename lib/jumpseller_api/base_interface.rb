module JumpsellerApi
  class BaseInterface
    def initialize(authentication)
      @authentication = authentication
    end

    def make_request(method, uri, params)
      Base.make_request(method, uri, params, @authentication)
    end
  end
end
