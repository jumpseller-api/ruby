module JumpsellerApi
  class CategoryInterface < BaseInterface
    def find_by(name:)
      Category.find_by(name, @authentication)
    end

    def find_or_create_by_name(name)
      Category.find_or_create_by_name(name, @authentication)
    end

    def find_all(&block)
      if block_given?
        Category.find_all(@authentication, &block)
      else
        Category.find_all(@authentication)
      end
    end
  end
end
