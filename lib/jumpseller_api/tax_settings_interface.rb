module JumpsellerApi
  class TaxSettingsInterface < BaseInterface
    def fetch
      TaxSettings.fetch(@authentication)
    end
  end
end
