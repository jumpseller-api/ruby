module JumpsellerApi
  class VariantCollection < Array
    attr_reader :raw

    def initialize(authentication, variants, product_id)
      @raw = variants
      @product_id = product_id
      @authentication = authentication
      @collection = initialize_variants(variants, @product_id)
      super(@collection)
    end

    def initialize_variants(variants, product_id)
      variants = fetch_api_variants || [] if variants.nil?
      collection = []
      variants.each do |variant|
        collection << Variant.new(variant, product_id, @authentication)
      end
      collection
    end

    def fetch_api_variants
      uri = "/products/#{@product_id}/variants.json"
      response = nil
      3.times do
        response = JumpsellerApi::Base.make_request('get', uri, {}, @authentication)
        break if response.success?

        sleep 1
      end
      return unless response.success?

      response.parsed_response
    end

    def find(id)
      @collection.find do |variant|
        variant.id.to_i == id.to_i
      end
    end

    def find_by(sku)
      @collection.each do |variant|
        return variant if variant.sku == sku
      end
      nil
    end
  end

  class Variant < JumpsellerApi::Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    attr_accessor :id, :sku, :stock, :stock_unlimited, :price, :options, :product_id, :raw, :weight, :discount, :image, :compare_at_price

    def resource
      "/products/#{@product_id}/variants"
    end

    def entity
      'variant'
    end

    def initialize(variant, product_id, authentication)
      @snapshot = nil
      @authentication = authentication
      @product_id = product_id

      self.attributes = if variant['variant']
                          (variant['variant'])
                        else
                          variant
                        end
    end

    def save
      # avoid compare_at_price error by never saving it
      # https://jumpseller.sentry.io/issues/4054098078/events/2c6246229a5a44b4a0ca84459f41fc89/?project=1230525&query=is%3Aunresolved&referrer=previous-event&statsPeriod=7d&stream_index=0
      @compare_at_price = nil
      @snapshot['compare_at_price'] = nil if @snapshot

      response = super(@authentication)
      return false unless response

      self.attributes = (response['variant'] || response)
    end

    def attributes=(variant)
      @snapshot = variant if variant['id']
      @raw = variant
      @id = variant['id']
      @sku = variant['sku']
      @stock = variant['stock']
      @stock_unlimited = variant['stock_unlimited']
      @price = variant['price']
      @options = variant['options']
      @discount = variant['discount']
      @weight = variant['weight']
      @image = variant['image']
      @compare_at_price = variant['compare_at_price']
    end

    def name
      options.map { |option| option['value'] }.join(' - ')
    end

    def attributes
      super(@snapshot).reject do |k, _v|
        k.to_sym == :product_id || k.to_sym == :raw
      end
    end
  end
end
