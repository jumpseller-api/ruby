module JumpsellerApi
  class Client
    attr_reader :store_id, :auth_token, :connection_type, :enviroment

    def initialize(store_id, auth_token, connection_type = 'oauth', enviroment = 'production')
      @store_id = store_id
      @auth_token = auth_token
      @connection_type = connection_type
      @enviroment = enviroment
    end

    def auth_params
      { login: @store_id, authtoken: @auth_token }
    end

    def product
      @product ||= JumpsellerApi::ProductInterface.new(self)
    end

    def order
      @order ||= JumpsellerApi::OrderInterface.new(self)
    end

    def order_history
      @order_history ||= JumpsellerApi::OrderHistoryInterface.new(self)
    end

    def category
      @category ||= JumpsellerApi::CategoryInterface.new(self)
    end

    def store
      @store ||= JumpsellerApi::StoreInterface.new(self)
    end

    def tax_settings
      @tax_settings ||= JumpsellerApi::TaxSettingsInterface.new(self)
    end
  end
end
