module JumpsellerApi
  class ProductInterface < BaseInterface
    def new(product)
      Product.new(product, @authentication)
    end

    def find(id)
      Product.find(id, @authentication)
    end

    def count
      Product.count(@authentication)
    end

    def find_all(&block)
      if block_given?
        Product.find_all(@authentication, &block)
      else
        Product.find_all(@authentication)
      end
    end

    def fetch_selected_ids(token)
      Product.fetch_selected_ids(token, @authentication)
    end
  end
end
