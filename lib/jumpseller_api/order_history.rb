module JumpsellerApi
  class OrderHistory < JumpsellerApi::Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    attr_accessor :hash

    def initialize(order_history, authentication)
      @authentication = authentication
      self.attributes = if order_history['order_history']
                          (order_history['order_history'])
                        else
                          order_history
                        end
    end

    def attributes=(order_history)
      @hash = order_history
    end
  end
end
