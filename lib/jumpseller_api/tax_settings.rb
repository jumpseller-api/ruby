module JumpsellerApi
  class TaxSettings < JumpsellerApi::Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    attr_reader :based_on, :tax_on_product_price, :tax_on_shipping_price

    def self.fetch(authentication)
      res = make_request('get', '/taxes/settings.json', {}, authentication)
      return nil if res.code == 403 || res.code == 405

      new(res.parsed_response)
    end

    def initialize(settings)
      self.attributes = settings
    end

    def attributes=(settings)
      @based_on = settings['based_on']
      @tax_on_product_price = settings['tax_on_product_price']
      @tax_on_shipping_price = settings['tax_on_shipping_price']
    end
  end
end
