require 'cgi'
module JumpsellerApi
  class Product < JumpsellerApi::Base
    include HTTParty
    base_uri JumpsellerApi::BASE_URI

    attr_accessor :id, :name, :description, :price, :sku, :weight, :stock, :stock_unlimited,
                  :featured, :status, :shipping_required, :created_at, :updated_at, :package_format,
                  :length, :width, :height, :diameter, :permalink, :categories, :images, :initialized_variants,
                  :brand, :barcode, :page_title, :discount, :google_product_category, :fields, :compare_at_price

    def self.count(authentication)
      res = make_request('get', '/products/count.json', {}, authentication)
      res.parsed_response['count']
    end

    def self.find(id, authentication)
      res = make_request('get', "/products/#{id}.json", {}, authentication)
      return nil if res.code == 404

      new(res.parsed_response['product'], authentication)
    end

    def self.find_all(authentication)
      raise ArgumentError, 'Missing mandatory block to yield.' unless block_given?

      products_count = count(authentication)
      params = { limit: 200, page: 1 }

      loop do
        parsed_result = make_request('get', '/products.json', { query: params }, authentication)&.parsed_response
        break if parsed_result.empty?

        parsed_result.each do |parsed_product|
          product = new(parsed_product['product'], authentication)

          yield(product, products_count)
        end

        params[:page] += 1
      end
    end

    def self.fetch_selected_ids(token, authentication)
      res = make_request('get', "/products/selected/#{token}.json", {}, authentication)

      return if res.code == 404

      res.parsed_response['product_ids']
    end

    def resource
      '/products'
    end

    def entity
      'product'
    end

    def initialize(product, authentication)
      @authentication = authentication
      @snapshot = nil
      product = (product || {}).deep_stringify_keys
      self.attributes = if product['product']
                          (product['product'])
                        else
                          product
                        end
    end

    def attributes=(product)
      product = (product || {}).deep_stringify_keys
      @snapshot = product if product['id']
      @id = product['id']
      @name = product['name']
      @description = product['description']
      @price = product['price']
      @sku = product['sku']
      @weight = product['weight']
      @stock = product['stock']
      @stock_unlimited = product['stock_unlimited']
      @featured = product['featured']
      @status = product['status']
      @shipping_required = product['shipping_required']
      @created_at = product['created_at']
      @updated_at = product['updated_at']
      @package_format = product['package_format']
      @length = product['length']
      @width = product['width']
      @height = product['height']
      @diameter = product['diameter']
      @permalink = product['permalink']
      @categories = product['categories']
      @images = product['images']
      @brand = product['brand']
      @barcode = product['barcode']
      @page_title = product['page_title']
      @discount = product['discount']
      @google_product_category = product['google_product_category']
      @compare_at_price = product['compare_at_price']
      @create_variants = true
      @variants = []
      @initialized_variants = nil

      @old_variants = @variants if product['variants']
      @snapshot_variants = product['variants'] if product['variants']
      @initialized_variants = product['variants'] if product['variants']
      @fields = product['fields'] if product['fields']
    end

    def variants
      @variants = @variants.empty? ? JumpsellerApi::VariantCollection.new(@authentication, @initialized_variants, @id) : @variants
    end

    def attributes
      attrs = super(@snapshot)
      at = attrs.reject do |k, _v|
        k.to_sym == :old_variants || k.to_sym == :create_variants || k.to_sym == :initialized_variants || (blank_variants? && k.to_sym == :variants)
      end

      at['variants'] = initialized_variants if !blank_variants? && @initialized_variants != @snapshot_variants
      at['variants'] = initialized_variants if !blank_variants? && !id
      at
    end

    def remove
      super(@authentication)
    end

    def save
      # avoid compare_at_price error by never saving it
      # https://jumpseller.sentry.io/issues/4054098078/events/2c6246229a5a44b4a0ca84459f41fc89/?project=1230525&query=is%3Aunresolved&referrer=previous-event&statsPeriod=7d&stream_index=0
      @compare_at_price = nil

      response = super(@authentication)
      return false unless response

      new_variants = []
      @id = response['product']['id']
      variants&.each do |variant|
        next unless variant.id || @create_variants

        variant.product_id = response['product']['id']
        variant.save
        new_variants << variant.raw
      end
      response['product']['variants'] = new_variants
      self.attributes = (response['product'])
      true
    end

    def blank_variants?
      initialized_variants.nil? || initialized_variants.empty?
    end
  end
end
