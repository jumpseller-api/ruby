# require_relative './jumpseller_api/client'
# require_relative './jumpseller_api/base'
# require_relative './jumpseller_api/product'
# require_relative './jumpseller_api/variant'
require 'httparty'
require 'active_support/core_ext/hash/indifferent_access'

require File.expand_path('./jumpseller_api/version', __dir__)
require File.expand_path('./jumpseller_api/base_uri', __dir__)
require File.expand_path('./jumpseller_api/exceptions', __dir__)

require File.expand_path('./jumpseller_api/base_interface', __dir__)
require File.expand_path('./jumpseller_api/store_interface', __dir__)
require File.expand_path('./jumpseller_api/product_interface', __dir__)
require File.expand_path('./jumpseller_api/order_interface', __dir__)
require File.expand_path('./jumpseller_api/order_history_interface', __dir__)
require File.expand_path('./jumpseller_api/category_interface', __dir__)
require File.expand_path('./jumpseller_api/tax_settings_interface', __dir__)

require File.expand_path('./jumpseller_api/client', __dir__)

require File.expand_path('./jumpseller_api/base', __dir__)
require File.expand_path('./jumpseller_api/product', __dir__)
require File.expand_path('./jumpseller_api/order', __dir__)
require File.expand_path('./jumpseller_api/order_history', __dir__)
require File.expand_path('./jumpseller_api/variant', __dir__)
require File.expand_path('./jumpseller_api/category', __dir__)
require File.expand_path('./jumpseller_api/store', __dir__)
require File.expand_path('./jumpseller_api/tax_settings', __dir__)

module JumpsellerApi
end
