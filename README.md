# Jumpseller API SDK

## Install
`git clone https://bitbucket.org/tiagomatos/jumpseller-api`

`cd jumpseller-api`

`gem install jumpseller_api-0.1.0.gem`



## Use

### connect client
`client = JumpsellerApi::Client.new(login, auth_token)`


### find all products of client store
`client.product.find_all`


### get on product of client store
`client.product.find(product_id)`


### remove a product
`product = client.product.find(product_id)`

`product.remove`


### create a product
`product = client.product.new({"name"=> "new product", "price" => 1})`

`product.save`


### update product attributes
`product = client.product.find(product_id)`

`product.name = "new name"`

`product.save`


### count products
`client.product.count`


### accessing variant of product
`product = client.product.find(product_id)`

`product.variants`
