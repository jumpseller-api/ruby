# frozen_string_literal: true

require './spec/spec_helper'

RSpec.describe 'Product' do
  let(:another_store_access_token) { 'oauthtoken-2' }
  let(:another_store_response_products) { File.new('./spec/responses/another_store_response_products.txt') }

  let(:store_access_token) { 'oauthtoken-1' }
  let(:store_response_products) { File.new('./spec/responses/store_response_products.txt') }
  let(:store_response_products_page_2) { File.new('./spec/responses/store_response_products_page_2.txt') }
  let(:store_response_1642993_variants) { File.new('./spec/responses/store_response_1642993_variants.txt') }
  let(:store_response_1642993) { File.new('./spec/responses/store_response_1642993.txt') }
  let(:store_response_1663399) { File.new('./spec/responses/store_response_1663399.txt') }

  let(:store_response_not_found_by_id_product) { File.new('./spec/responses/store_response_not_found_by_id_product.txt') }
  let(:store_response_create_product) { File.new('./spec/responses/store_response_create_product.txt') }
  let(:store_response_create_product_with_variants) { File.new('./spec/responses/store_response_create_product_with_variants.txt') }
  let(:store_response_create_variants_for_1653816) { File.new('./spec/responses/store_response_create_variants_for_1653816.txt') }
  let(:store_response_new_1653797_empty_variants) { File.new('./spec/responses/store_response_new_1653797_empty_variants.txt') }
  let(:store_response_new_1653797_variants) { File.new('./spec/responses/store_response_new_1653797_variants.txt') }
  let(:store_response_sku_12458) { File.new('./spec/responses/store_response_sku_12458.txt') }
  let(:store_response_sku_not_found) { File.new('./spec/responses/store_response_sku_not_found.txt') }
  let(:store_response_sku_polera_azul_p) { File.new('./spec/responses/store_response_sku_polera_azul_p.txt') }
  let(:store_response_sku_repeated_sku) { File.new('./spec/responses/store_response_sku_repeated_sku.txt') }
  let(:store_response_variants_1380011) { File.new('./spec/responses/store_response_variants_1380011.txt') }
  let(:store_response_variants_3333303) { File.new('./spec/responses/store_response_variants_3333303.txt') }
  let(:store_response_find_by_sku_not_and_found) { File.new('./spec/responses/store_response_find_by_sku_not_and_found.txt') }
  let(:store_response_find_by_sku_galeton) { File.new('./spec/responses/store_response_find_by_sku_galeton.txt') }
  let(:store_response_update_product_1663399) { File.new('./spec/responses/store_response_update_product_1663399.txt') }
  let(:store_response_update_product_1642993) { File.new('./spec/responses/store_response_update_product_1642993.txt') }
  let(:store_response_product_1663511) { File.new('./spec/responses/store_response_product_1663511.txt') }
  let(:store_response_products_count) { File.new('./spec/responses/store_response_products_count.txt') }
  let(:another_store_response_products_count) { File.new('./spec/responses/another_store_response_products_count.txt') }
  let(:store_response_product_token_demostore_product_selection_1) { File.new('./spec/responses/store_response_product_token_ demostore_product_selection_1.txt') }

  let(:store_response_product_remove_1663511) { File.new('./spec/responses/store_response_product_remove_1663511.txt') }
  let(:rate_limit_exceeded) { File.new('./spec/responses/rate_limit_exceeded.txt') }
  let(:store_response_auth_error) { File.new('./spec/responses/store_response_auth_error.txt') }
  let(:store_response_cant_create_product) { File.new('./spec/responses/store_response_cant_create_product.txt') }

  let(:new_product_params) { { product: { name: 'test', price: 1000 } } }

  let(:new_product_with_errors) { { product: { name: 'test' } } }
  let(:product_with_compare_at_price) { { product: { price: 1000, compare_at_price: 800 } } }

  let(:variant_params) { { price: 5000, options: [{ name: 'tala', value: 'XXL' }] } }
  let(:new_product_with_variants_params) { { product: { name: 'polera', variants: [variant_params] } } }
  let(:authentication) { JumpsellerApi::Client.new('oauth', store_access_token) }

  before(:each) do
    stub_requests_for_product
  end

  describe '.make_request' do
    let(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }
    let(:request) { store_product_api.make_request('get', 'https://api.jumpseller.com/v1/products/count.json', {}) }

    it 'store must have 12 products' do
      count = request.parsed_response['count']
      expect(count).to eq(12)
    end
  end
  describe '#remove' do
    let(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }

    context 'existant product' do
      let(:product) { store_product_api.find('1663511') }
      it 'must return hash after removed' do
        expect(product.remove).to be_kind_of(Hash)
      end

      it 'response message must be ok' do
        expect(product.remove['message']).to eq('OK')
      end
    end

    context 'not existant product' do
      let(:product) { store_product_api.new(name: 'Unexistant') }

      it 'must return hash after removed' do
        expect(product.remove).to be_kind_of(Hash)
      end

      it 'response message must be failed' do
        expect(product.remove['message']).to eq('Failed')
      end
    end
  end

  describe '#save' do
    describe 'update' do
      let(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }

      context 'valid product' do
        let(:product) { store_product_api.find('1663399') }

        it 'product id must be' do
          product.name = 'Product Updated'
          product.save
          expect(product.id).to eq(1_663_399)
        end

        it 'must changes name' do
          expect do
            product.name = 'Product Updated'
            product.save
          end.to change { product.name }.from('Product will be updated').to('Product Updated')
        end

        it 'does not change compare_at_price' do
          product.name = 'Product Updated'
          product.price = 7000.0
          product.compare_at_price = 6000.0
          product.save
          expect(product.compare_at_price).to be_nil
        end
      end

      context 'with variants' do
        let(:product_with_variants) { store_product_api.find('1642993') }

        it 'does not change compare_at_price' do
          product_with_variants.name = 'Product Updated'
          product_with_variants.variants.first.price = 7000.0
          product_with_variants.variants.first.compare_at_price = 6000.0
          product_with_variants.save
          expect(product_with_variants.variants.first.compare_at_price).to be_nil
        end
      end
    end

    describe 'create' do
      context 'invalid product' do
        let!(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }
        let(:new_product) { store_product_api.new(new_product_with_errors) }
        it 'must raise API ERROR' do
          expect { new_product.save }.to raise_error(JumpsellerApi::CouldNotSave)
        end
      end
      context 'valid product' do
        let!(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }

        describe 'without variants and API returns [] variants' do
          let(:new_product) { store_product_api.new(new_product_params) }

          before do
            stub_request(:get, 'https://api.jumpseller.com/v1/products/1653797/variants.json')
              .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
              .to_return(store_response_new_1653797_empty_variants)
          end

          it 'product must have an id' do
            new_product.save
            expect(new_product.id).to_not be_nil
          end

          it 'product id must be' do
            new_product.save
            expect(new_product.id).to eq(1_653_797)
          end
        end

        describe 'without variants, but API returns variants' do
          let(:new_product) { store_product_api.new(new_product_params) }

          before do
            stub_request(:get, 'https://api.jumpseller.com/v1/products/1653797/variants.json')
              .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
              .to_return(store_response_new_1653797_variants)
          end

          it 'product must have variants' do
            new_product.save
            expect(new_product.variants).to_not be_empty
          end
        end

        describe 'with variants' do
          let(:new_product_with_variants) { store_product_api.new(new_product_with_variants_params) }
          it 'product must have an id' do
            new_product_with_variants.save
            expect(new_product_with_variants.id).to_not be_nil
          end

          it 'product id must be' do
            new_product_with_variants.save
            expect(new_product_with_variants.id).to eq(1_653_816)
          end

          it "product name must be 'polera'" do
            new_product_with_variants.save
            expect(new_product_with_variants.name).to eq('polera')
          end

          it "variant name must be 'XXL'" do
            new_product_with_variants.save
            variant = new_product_with_variants.variants.last
            expect(variant.name).to eq('XXL')
          end
        end
      end
    end
  end

  describe '.count' do
    context 'store has products' do
      let!(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }
      it 'store must have 6 products' do
        expect(store_product_api.count).to eq(12)
      end
    end

    context 'store has no products' do
      let!(:store_product_api) { JumpsellerApi::Client.new('oauth', another_store_access_token).product }
      it 'store must have 0 products' do
        expect(store_product_api.count).to eq(0)
      end
    end
  end

  describe '.new' do
    let!(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }
    let(:product) { store_product_api.new(product: { name: 'test' }) }

    it 'product must to be a product instance' do
      expect(product).to be_kind_of(JumpsellerApi::Product)
    end

    it "product must to have name 'test'" do
      expect(product.name).to eq('test')
    end
  end

  describe '.find' do
    let!(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }
    context 'rate_limit_exceeded' do
      let(:product) { store_product_api.find('rate_limit_exceeded') }

      it 'must raise rate limit exceeded' do
        expect { product }.to raise_error(JumpsellerApi::RateLimitExceeded)
      end
    end

    context 'wrong token' do
      let(:product) { store_product_api.find('auth_error') }

      it 'must raise wrong oauth token' do
        expect { product }.to raise_error(JumpsellerApi::WrongOauth2TokenProvided)
      end
    end
    context 'found product' do
      let(:product) { store_product_api.find('1642993') }

      it 'product must to be a product instance' do
        expect(product).to be_kind_of(JumpsellerApi::Product)
      end

      it "product must have name 'NOVO PRODUTO COM VARIANTE'" do
        expect(product.name).to eq('NOVO PRODUTO COM VARIANTE')
      end

      describe "find variant '3333303'" do
        let(:variant) { product.variants.find('3333303') }

        it 'must be a variant instance' do
          expect(variant).to be_kind_of(JumpsellerApi::Variant)
        end

        it "must have name 'NOVO PRODUTO COM VARIANTE'" do
          expect(variant.name).to eq('G')
        end
      end
    end

    context 'not found product' do
      let(:product) { store_product_api.find('000') }

      it 'product must be nil' do
        expect(product).to be_kind_of(NilClass)
      end
    end
  end

  describe '.find_all' do
    let(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }

    context 'when without block' do
      it 'returns exception' do
        expect { store_product_api.find_all }.to raise_error(ArgumentError)
      end
    end

    context 'with block given and products on store' do
      it 'returns a product instance' do
        products = []
        store_product_api.find_all do |product, _products_count|
          products << product
        end

        expect(products.last).to be_kind_of(JumpsellerApi::Product)
      end
    end

    context 'when it has variants of product' do
      let(:products) { [] }
      let(:last_product) { products.last }

      before do
        store_product_api.find_all do |product|
          products << product
        end
      end

      it 'variants collection must to be a VariantCollection instance' do
        expect(last_product.variants).to be_kind_of(JumpsellerApi::VariantCollection)
      end

      it 'variants collection must to have a variant instance' do
        expect(last_product.variants.last).to be_kind_of(JumpsellerApi::Variant)
      end

      it 'last variant has attribute name' do
        expect(last_product.variants.last.name).to_not be_empty
      end
    end
  end

  describe '.fetch_selected_ids' do
    let(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }
    let(:product_ids) { store_product_api.fetch_selected_ids('demostore_product_selection_1') }

    it 'found token and returns the array of ids' do
      expect(product_ids).to eq [1]
    end
  end

  context 'when two stores try to access store api at the same time' do
    let!(:another_store_product_api) { JumpsellerApi::Client.new('oauth', another_store_access_token).product }
    let!(:store_product_api) { JumpsellerApi::Client.new('oauth', store_access_token).product }
    let(:store_products) { [] }
    let(:another_store_products) { [] }

    before do
      store_product_api.find_all do |p|
        store_products << p
      end

      another_store_product_api.find_all do |p|
        another_store_product_api << p
      end
    end

    it 'jumpseller apps test store must get an empty collection of products' do
      expect(another_store_products).to be_empty
    end

    it 'store must get a collection of products' do
      expect(store_products).to_not be_empty
    end
  end
end

def stub_requests_for_product
  stub_request(:get, 'https://api.jumpseller.com/v1/products.json?limit=200&page=1')
    .with(headers: { 'Authorization' => "Bearer #{another_store_access_token}" })
    .to_return(another_store_response_products)

  stub_request(:get, 'https://api.jumpseller.com/v1/products.json?limit=200&page=1')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_products)

  stub_request(:get, 'https://api.jumpseller.com/v1/products.json?limit=200&page=2')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_products_page_2)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/count.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_products_count)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/count.json')
    .with(headers: { 'Authorization' => "Bearer #{another_store_access_token}" })
    .to_return(another_store_response_products_count)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/1642993/variants.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_1642993_variants)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/1642993.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_1642993)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/1663399.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_1663399)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/1663511.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_product_1663511)

  stub_request(:delete, 'https://api.jumpseller.com/v1/products/1663511.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_product_remove_1663511)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/000.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_not_found_by_id_product)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/search.json?query=12458')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_sku_12458)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/search.json?query=not_found')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_sku_not_found)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/search.json?query=polera_azul_p')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_sku_polera_azul_p)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/search.json?query=repeated+sku')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_sku_repeated_sku)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/search.json?query=not%26found')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_find_by_sku_not_and_found)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/search.json?query=Gallet%C3%B3n%20de%20Vainilla')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_find_by_sku_galeton)

  stub_request(:post, 'https://api.jumpseller.com/v1/products.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" }, body: new_product_params.to_json)
    .to_return(store_response_create_product)

  stub_request(:post, 'https://api.jumpseller.com/v1/products.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" }, body: new_product_with_variants_params.to_json)
    .to_return(store_response_create_product_with_variants)

  stub_request(:post, 'https://api.jumpseller.com/v1/products.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" }, body: new_product_with_errors.to_json)
    .to_return(store_response_cant_create_product)

  stub_request(:post, 'https://api.jumpseller.com/v1/products/1653816/variants.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" }, body: { variant: variant_params }.to_json)
    .to_return(store_response_create_variants_for_1653816)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/1380011/variants.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_variants_1380011)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/rate_limit_exceeded.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(rate_limit_exceeded)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/auth_error.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_auth_error)

  stub_request(:put, 'https://api.jumpseller.com/v1/products/1663399.json').with do |request|
    request.headers['Authorization'] == "Bearer #{store_access_token}" && JSON.parse(request.body)['product']['name'] == 'Product Updated'
  end.to_return(store_response_update_product_1663399)

  stub_request(:put, 'https://api.jumpseller.com/v1/products/1642993.json').with do |request|
    request.headers['Authorization'] == "Bearer #{store_access_token}" && JSON.parse(request.body)['product']['name'] == 'Product Updated'
  end.to_return(store_response_update_product_1642993)

  stub_request(:get, 'https://api.jumpseller.com/v1/products/selected/demostore_product_selection_1.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_product_token_demostore_product_selection_1)

  stub_request(:put, 'https://api.jumpseller.com/v1/products/1642993/variants/3333303.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" }, body: { variant: { price: 7000.0 } }.to_json)
    .to_return(store_response_variants_3333303)
end

# curl -is -X GET -H "Authorization: Bearer oauthtoken-1" https://api.jumpseller.com/v1/products.json?page=2 > spec/responses/store_response_products_page_2.txt

# Rspec.describe Product do
#   describe ''
#   let(:another_store_access_token) { 'oauthtoken-2' }
#   let(:another_store_response_products) { File.new("../responses/another_store_response_products.txt") }
#
#   let(:store_access_token) { 'oauthtoken-1' }
#   let(:store_response_products) { File.new("../responses/store_response_products.txt") }
#   let(:store_response_products_page_2) { File.new("../responses/store_response_products_page_2.txt") }
#
#   stub_request(:get, "https://api.jumpseller.com/v1/products.json").
#     with({headers: {'Authorization'=>"Bearer #{another_store_access_token}"}}).
#     to_return(another_store_response_products)
#
#   stub_request(:get, "https://api.jumpseller.com/v1/products.json").
#     with({headers: {'Authorization'=>"Bearer #{store_access_token}"}}).
#     to_return(store_response_products)
#
#   stub_request(:get, "https://api.jumpseller.com/v1/products.json?page=2").
#     with({headers: {'Authorization'=>"Bearer #{store_access_token}"}}).
#     to_return(store_response_products_page_2)
#
#
# end
#
#
# #curl -is -X GET -H "Authorization: Bearer oauthtoken-1" https://api.jumpseller.com/v1/products.json?page=2 > spec/responses/store_response_products_page_2.txt
