require './spec/spec_helper'

RSpec.describe 'Category' do
  let(:store_access_token) { 'oauthtoken-1' }

  let(:store_response_categories) { File.new('./spec/responses/store_response_categories.txt') }
  let(:store_response_new_category) { File.new('./spec/responses/store_response_new_category.txt') }
  let(:store_response_nova_categoria) { File.new('./spec/responses/store_response_nova_categoria.txt') }

  let(:category_api) { JumpsellerApi::Client.new('oauth', store_access_token).category }

  before(:each) do
    stub_category_requests
  end

  describe '.find_all' do
    let(:categories) { category_api.find_all }

    it 'must be an array' do
      expect(categories).to be_kind_of(Array)
    end

    it 'collection must have category instances elmenents' do
      expect(categories.last).to be_kind_of(JumpsellerApi::Category)
    end

    context 'with given block' do
      it 'collection must have order instances elmenents' do
        categories_array = []
        category_api.find_all do |category|
          categories_array << category
        end
        expect(categories_array.last).to be_kind_of(JumpsellerApi::Category)
      end
    end
  end

  describe '.find_or_create_by_name' do
    context 'when category name is blank' do
      let(:category) { category_api.find_or_create_by_name('') }
      it 'must be a category instance' do
        expect(category).to be_nil
      end
    end

    context 'when category name is nil' do
      let(:category) { category_api.find_or_create_by_name(nil) }
      it 'must be a category instance' do
        expect(category).to be_nil
      end
    end

    context 'create when category was not found' do
      context "category 'Nova categoría' was found" do
        let(:category) { category_api.find_or_create_by_name('Nova Categoría') }
        it 'must be a category instance' do
          expect(category).to be_kind_of(JumpsellerApi::Category)
        end

        it "must be named as 'Nova Categoría'" do
          expect(category.name).to eq('Nova Categoría')
        end

        it "id must be '228810'" do
          expect(category.id).to eq(228_810)
        end
      end

      context "category 'New Category' was found" do
        let(:category) { category_api.find_or_create_by_name('New Category') }
        it 'must be a category instance' do
          expect(category).to be_kind_of(JumpsellerApi::Category)
        end

        it "must be named as 'New Category'" do
          expect(category.name).to eq('New Category')
        end

        it "id must be '228809'" do
          expect(category.id).to eq(228_809)
        end
      end
    end

    context 'dont create when category was found' do
      context "category 'Perfume' was found" do
        let(:category) { category_api.find_or_create_by_name('Perfume') }
        it 'must be a category instance' do
          expect(category).to be_kind_of(JumpsellerApi::Category)
        end

        it "must be named as 'Perfume'" do
          expect(category.name).to eq('Perfume')
        end

        it "id must be '216036'" do
          expect(category.id).to eq(216_036)
        end
      end
    end
  end

  describe '.find_by_name' do
    context "category 'Perfume' was found" do
      let(:category) { category_api.find_by(name: 'Perfume') }
      it 'must be a category instance' do
        expect(category).to be_kind_of(JumpsellerApi::Category)
      end

      it "must be named as 'Perfume'" do
        expect(category.name).to eq('Perfume')
      end

      it "id must be '216036'" do
        expect(category.id).to eq(216_036)
      end
    end

    context "category 'not found' was not found" do
      let(:category) { category_api.find_by(name: 'not found') }
      it 'must be nil' do
        expect(category).to be_nil
      end
    end

    context "category 'not&found' was not found" do
      let(:category) { category_api.find_by(name: 'not&found') }
      it 'must be nil' do
        expect(category).to be_nil
      end
    end

    context "category 'not\found' was not found" do
      let(:category) { category_api.find_by(name: 'not\found') }
      it 'must be nil' do
        expect(category).to be_nil
      end
    end
  end
end

def stub_category_requests
  stub_request(:get, 'https://api.jumpseller.com/v1/categories.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_categories)

  stub_request(:post, 'https://api.jumpseller.com/v1/categories.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" }, body: { category: { name: 'New Category' } }.to_json)
    .to_return(store_response_new_category)

  stub_request(:post, 'https://api.jumpseller.com/v1/categories.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" }, body: { category: { name: 'Nova Categoría' } }.to_json)
    .to_return(store_response_nova_categoria)
end

# curl -is -X GET -H "Authorization: Bearer oauthtoken-1" https://api.jumpseller.com/v1/products.json?page=2 > spec/responses/store_response_products_page_2.txt
