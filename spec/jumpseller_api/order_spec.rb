require './spec/spec_helper'

RSpec.describe 'Order' do
  let(:store_access_token) { 'oauthtoken-1' }

  let(:store_response_orders) { File.new('./spec/responses/store_response_orders.txt') }
  let(:store_response_orders_page_2) { File.new('./spec/responses/store_response_orders_page_2.txt') }
  let(:store_response_orders_count) { File.new('./spec/responses/store_response_orders_count.txt') }
  let(:store_response_order_1019) { File.new('./spec/responses/store_response_order_1019.txt') }
  let(:store_response_order_not_found) { File.new('./spec/responses/store_response_order_not_found.txt') }

  let(:order_api) { JumpsellerApi::Client.new('oauth', store_access_token).order }

  before(:each) do
    stub_order_requests
  end

  let!(:store_api) { JumpsellerApi::Client.new('oauth', store_access_token).store }

  describe 'initialize' do
    let(:order_params) { { 'order' => { 'id' => 1019 } } }
    let(:order) { order_api.new(order_params) }

    it 'must be an order instance' do
      expect(order).to be_kind_of(JumpsellerApi::Order)
    end
  end

  describe '#resource' do
    let(:order) { order_api.find('1019') }

    it "must be '/orders'" do
      expect(order.resource).to eq('/orders')
    end
  end

  describe '#entity' do
    let(:order) { order_api.find('1019') }
    it "must be 'order'" do
      expect(order.entity).to eq('order')
    end
  end

  describe '.find' do
    describe 'found 1019 found' do
      let(:order) { order_api.find('1019') }

      it 'must be an order instance' do
        expect(order).to be_kind_of(JumpsellerApi::Order)
      end
    end

    describe 'not found' do
      let(:order) { order_api.find('not_found') }
      it 'must be nil' do
        expect(order).to be_nil
      end
    end
  end

  describe '.find_all' do
    let(:orders) { order_api.find_all }

    it 'must be an array' do
      expect(orders).to be_kind_of(Array)
    end

    it 'collection must have order instances elmenents' do
      expect(orders.last).to be_kind_of(JumpsellerApi::Order)
    end

    context 'with given block' do
      it 'collection must have order instances elmenents' do
        orders_array = []
        order_api.find_all do |order, _order_count|
          orders_array << order
        end
        expect(orders_array.last).to be_kind_of(JumpsellerApi::Order)
      end
    end
  end
end

def stub_order_requests
  stub_request(:get, 'https://api.jumpseller.com/v1/orders.json?limit=200&page=1')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_orders)

  stub_request(:get, 'https://api.jumpseller.com/v1/orders.json?limit=200&page=2')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_orders_page_2)

  stub_request(:get, 'https://api.jumpseller.com/v1/orders/count.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_orders_count)

  stub_request(:get, 'https://api.jumpseller.com/v1/orders/1019.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_order_1019)

  stub_request(:get, 'https://api.jumpseller.com/v1/orders/not_found.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_order_not_found)
end

# curl -is -X GET -H "Authorization: Bearer oauthtoken-1" https://api.jumpseller.com/v1/products.json?page=2 > spec/responses/store_response_products_page_2.txt
