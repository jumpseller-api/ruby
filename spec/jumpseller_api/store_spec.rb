require './spec/spec_helper'

RSpec.describe 'Store' do
  let(:login) { 'login' }
  let(:authtoken) { 'authtoken' }

  let(:another_store_access_token) { 'oauthtoken-2' }
  let(:another_store_response_info) { File.new('./spec/responses/another_store_response_info.txt') }

  let(:store_access_token) { 'oauthtoken-1' }
  let(:store_response_info) { File.new('./spec/responses/store_response_info.txt') }

  let(:store_response_with_login_method) { File.new('./spec/responses/store_response_with_login_method.txt') }

  let(:store_access_token_address) { 'oauthtoken-3' }
  let(:store_response_info_address) { File.new('./spec/responses/store_response_info_address.txt') }

  before(:each) do
    stub_store_requests
  end
  let!(:store_api) { JumpsellerApi::Client.new('oauth', store_access_token).store }

  describe '#resource' do
    it "must be '/store/info'" do
      expect(store_api.info.resource).to eq('/store/info')
    end
  end

  describe '#entity' do
    it "must be 'store'" do
      expect(store_api.info.entity).to eq('store')
    end
  end

  describe '.info' do
    describe 'with login method' do
      let(:store_api) { JumpsellerApi::Client.new(login, authtoken, 'login').store }

      it "store must get 'yuri' code" do
        expect(store_api.info.code).to eq('yuri')
      end
    end

    describe 'with address method' do
      let(:store_api) { JumpsellerApi::Client.new('oauth', store_access_token_address).store }
      let(:address) do
        {
          'address' => 'Rua de Sá da Bandeira 706',
          'city' => 'Porto',
          'country' => 'Chile',
          'country_code' => 'CL',
          'postal' => '4000-429 Porto',
          'region' => nil,
          'region_code' => nil
        }
      end

      it 'store must get address hash' do
        expect(store_api.info.address).to eq address
      end
    end

    context 'one client try to get his own store code' do
      let!(:another_store_api) { JumpsellerApi::Client.new('oauth', another_store_access_token).store }

      it "jumpseller apps test store must get 'jumpseller-apps-test' code" do
        expect(another_store_api.info.code).to eq('jumpseller-apps-test')
      end
    end

    context 'when two stores tries to access store api at the same time' do
      let!(:another_store_api) { JumpsellerApi::Client.new('oauth', another_store_access_token).store }
      let!(:store_api) { JumpsellerApi::Client.new('oauth', store_access_token).store }

      it "jumpseller apps test store must get 'jumpseller-apps-test' code" do
        expect(another_store_api.info.code).to eq('jumpseller-apps-test')
      end
    end
  end
end

def stub_store_requests
  stub_request(:get, 'https://api.jumpseller.com/v1/store/info.json')
    .with(headers: { 'Authorization' => "Bearer #{another_store_access_token}" })
    .to_return(another_store_response_info)

  stub_request(:get, 'https://api.jumpseller.com/v1/store/info.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token}" })
    .to_return(store_response_info)

  stub_request(:get, 'https://api.jumpseller.com/v1/store/info.json')
    .with(headers: { 'Authorization' => "Bearer #{store_access_token_address}" })
    .to_return(store_response_info_address)

  stub_request(:get, "https://api.jumpseller.com/v1/store/info.json?login=#{login}&authtoken=#{authtoken}")
    .to_return(store_response_with_login_method)
end

# curl -is -X GET -H "Authorization: Bearer oauthtoken-1" https://api.jumpseller.com/v1/products.json?page=2 > spec/responses/store_response_products_page_2.txt
