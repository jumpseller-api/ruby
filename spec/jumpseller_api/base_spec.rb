require './spec/spec_helper'

RSpec.describe 'Base' do
  describe '.fetch_sleep_seconds' do
    subject { JumpsellerApi::Base.fetch_sleep_seconds(per_second_limit, remaining_per_second, per_minute_limit, remaining_per_minute) }
    context 'plan premium with 4 requests per second and 120 per minute' do
      let(:per_minute_limit) { 120 }
      let(:per_second_limit) { 4 }

      context 'with 4 remaining requests per second and 120 remaining requests per minute' do
        let(:remaining_per_second) { 4 }
        let(:remaining_per_minute) { 120 }

        it { expect(subject).to eq(0.25) }
      end

      context 'with 3 remaining requests per second and 50 remaining requests per minute' do
        let(:remaining_per_second) { 3 }
        let(:remaining_per_minute) { 50 }

        it { expect(subject).to eq(0.3535533905932738) }
      end

      context 'with 3 remaining requests per second and 10 remaining requests per minute' do
        let(:remaining_per_second) { 3 }
        let(:remaining_per_minute) { 10 }

        it { expect(subject).to eq(0.6710191216109036) }
      end

      context 'with 2 remaining requests per second and 10 remaining requests per minute' do
        let(:remaining_per_second) { 2 }
        let(:remaining_per_minute) { 10 }

        it { expect(subject).to eq(0.6710191216109036) }
      end

      context 'with 1 remaining requests per second and 20 remaining requests per minute' do
        let(:remaining_per_second) { 1 }
        let(:remaining_per_minute) { 20 }

        it { expect(subject).to eq(0.7071067811865476) }
      end

      context 'with 4 remaining requests per second and 0 remaining requests per minute' do
        let(:remaining_per_second) { 4 }
        let(:remaining_per_minute) { 0 }

        it { expect(subject).to eq(1.0) }
      end

      context 'with 0 remaining requests per second and 0 remaining requests per minute' do
        let(:remaining_per_second) { 0 }
        let(:remaining_per_minute) { 0 }

        it { expect(subject).to eq(1.0) }
      end

      context 'with 0 remaining requests per second and 120 remaining requests per minute' do
        let(:remaining_per_second) { 0 }
        let(:remaining_per_minute) { 120 }

        it { expect(subject).to eq(1.0) }
      end
    end

    context 'plan pro with 2 requests per second and 60 per minute' do
      let(:per_second_limit) { 2 }
      let(:per_minute_limit) { 60 }

      context 'with 2 remaining requests per second and 60 remaining requests per minute' do
        let(:remaining_per_second) { 2 }
        let(:remaining_per_minute) { 60 }

        it { expect(subject).to eq(0.5) }
      end

      context 'with 1 remaining requests per second and 60 remaining requests per minute' do
        let(:remaining_per_second) { 1 }
        let(:remaining_per_minute) { 60 }

        it { expect(subject).to eq(0.7071067811865476) }
      end

      context 'with 1 remaining requests per second and 30 remaining requests per minute' do
        let(:remaining_per_second) { 1 }
        let(:remaining_per_minute) { 30 }

        it { expect(subject).to eq(0.7071067811865476) }
      end

      context 'with 0 remaining requests per second and 60 remaining requests per minute' do
        let(:remaining_per_second) { 0 }
        let(:remaining_per_minute) { 60 }

        it { expect(subject).to eq(1.0) }
      end

      context 'with 2 remaining requests per second and 0 remaining requests per minute' do
        let(:remaining_per_second) { 2 }
        let(:remaining_per_minute) { 0 }

        it { expect(subject).to eq(1.0) }
      end

      context 'with 0 remaining requests per second and 0 remaining requests per minute' do
        let(:remaining_per_second) { 0 }
        let(:remaining_per_minute) { 0 }

        it { expect(subject).to eq(1.0) }
      end

      context 'with 2 remaining requests per second and 10 remaining requests per minute' do
        let(:remaining_per_second) { 2 }
        let(:remaining_per_minute) { 10 }

        it { expect(subject).to eq(0.5054072392284441) }
      end

      context 'with 1 remaining requests per second and 20 remaining requests per minute' do
        let(:remaining_per_second) { 2 }
        let(:remaining_per_minute) { 10 }

        it { expect(subject).to eq(0.5054072392284441) }
      end
    end
  end
end
