HTTP/1.1 200 OK
Server: openresty/1.13.6.1
Date: Fri, 30 Mar 2018 20:51:20 GMT
Content-Type: application/json
Content-Length: 264
Connection: keep-alive
Jumpseller-PerMinuteRateLimit-Limit: 60
Jumpseller-PerMinuteRateLimit-Remaining: 59
Jumpseller-PerSecondRateLimit-Limit: 2
Jumpseller-PerSecondRateLimit-Remaining: 1
X-Content-Type-Options: nosniff

{"store":{"name":"yuri","code":"yuri","currency":"CLP","country":"CL","timezone":"Pacific/Easter","email":"yuri.correa+test@jumpseller.com","hooks_token":"9cafdf8c00f69c91c947d9302cbec43f","logo":null,"url":"https://yuri.jumpseller.com","subscription_plan":"pro"}}