require File.expand_path('lib/jumpseller_api/version', __dir__)
Gem::Specification.new do |s|
  s.name        = 'jumpseller_api'
  s.version     = JumpsellerApi::VERSION
  s.date        = '2017-08-04'
  s.summary     = 'Jumpseller API SDK'
  s.description = 'Gem for Jumpseller API SDK'
  s.authors     = ['Yuri Mello']
  s.email       = 'yuri.correa@jumpseller.com'
  s.files       = `git ls-files lib`.split($INPUT_RECORD_SEPARATOR)
  s.homepage    =
    'https://bitbucket.org/tiagomatos/jumpseller-api'
  s.license       = 'MIT'
  s.require_paths = ['lib']
  s.add_dependency 'httparty'
end
